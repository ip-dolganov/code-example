from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


__author__ = 'Nikolay Dolganov'


class CodexUsersConfig(AppConfig):
    """
    Configure codex_users
    """

    name = 'codex_users'
    verbose_name = _('Users')

    def ready(self):
        super().ready()


__all__ = ['CodexUsersConfig']
