from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


__author__ = 'Nikolay Dolganov'


class CodexUser(AbstractUser):
    """
    Custom User model for codex
    """

    patronymic = models.CharField(verbose_name=_('Patronymic'), max_length=255, blank=True, null=True)

    class Meta(AbstractUser.Meta):
        abstract = False

    def __str__(self):
        return self.username