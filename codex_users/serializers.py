from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.contrib.auth import authenticate
from django.utils.translation import gettext_lazy as _
from validate_email import validate_email


__author__ = 'Nikolay Dolganov'
User = get_user_model()


class UserBaseSerializer(serializers.ModelSerializer):
    """
    Serialize user's base info
    """

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'patronymic', 'last_name', 'is_staff', 'is_superuser']


class UserAuthSerializer(UserBaseSerializer):
    """
    Serialize User after the authentication
    """

    token = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    def get_token(self, instance):
        return instance.auth_token.key if hasattr(instance, 'auth_token') else None

    def get_user(self, instance):
        serializer = UserBaseSerializer(instance)
        return serializer.data

    class Meta:
        model = User
        fields = ['user', 'token']


class SignInSerializer(serializers.Serializer):
    """
    Serialize sign-in
    User can use his username or email
    """

    login = serializers.CharField(max_length=255, min_length=3)
    password = serializers.CharField(max_length=255)

    base_error_mess = _('Incorrect login or password')
    user = None

    def __get_user_or_rise(self, login_str):
        """
        Validate login_str and find user or rise serializers.ValidationError
        :param login_str: str
        :return: User instance
        """
        filter_kw = {'email__iexact': login_str} if validate_email(login_str) else {'username': login_str}
        filter_qs = User.objects.filter(**filter_kw)
        if not filter_qs.exists():
            raise serializers.ValidationError(self.base_error_mess)
        else:
            return filter_qs.first()

    def validate(self, attrs):
        username = self.__get_user_or_rise(attrs['login']).username
        user = authenticate(username=username, password=attrs['password'])
        if user and user.is_active:
            self.user = user
            return attrs
        else:
            raise serializers.ValidationError(self.base_error_mess)

    class Meta:
        fields = ['login', 'password']