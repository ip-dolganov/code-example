from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from django.contrib.auth import get_user_model

__author__ = 'Nikolay Dolganov'
User = get_user_model()


class TestSignIn(APITestCase):
    """
    Test SignIn mechanism
    """

    sign_in_url = reverse('codex_users:sign_in')
    headers = {'DO_NOT_LOGIN': True}

    def setUp(self):
        user = User.objects.create(email='sirnikolasd@gmail.com')
        user.set_password('qwerty1234')
        user.save()
        user = User.objects.create(email='sirnikolasd1@gmail.com', username='test-user')
        user.set_password('qwerty1234')
        user.save()

    def test_authentication_email(self):
        sign_in_response = self.client.post(self.sign_in_url, {'login': 'sirnikolasd@gmail.com',
                                                               'password': 'qwerty1234'}, **self.headers)
        self.assertEqual(sign_in_response.status_code, status.HTTP_200_OK)
        self.assertIn('token', sign_in_response.data.keys())
        self.assertIsNotNone(sign_in_response.data['token'])

    def test_authentication_login(self):
        sign_in_response = self.client.post(self.sign_in_url, {'login': 'test-user',
                                                               'password': 'qwerty1234'}, **self.headers)
        self.assertEqual(sign_in_response.status_code, status.HTTP_200_OK)
        self.assertIn('token', sign_in_response.data.keys())

    def test_incorrect_login(self):
        sign_in_response = self.client.post(self.sign_in_url, {'login': 'non-existing-user',
                                                               'password': 'qwerty1234'}, **self.headers)
        self.assertEqual(sign_in_response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertListEqual(['non_field_errors'], list(sign_in_response.data.keys()))
        sign_in_response = self.client.post(self.sign_in_url, {'login': 'test-user',
                                                               'password': 'qwerty12345'}, **self.headers)
        self.assertEqual(sign_in_response.status_code, status.HTTP_400_BAD_REQUEST)