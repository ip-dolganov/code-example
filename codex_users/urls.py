from django.conf.urls import url
from codex_users.views import SignInAPIView, LogOutAPIView

__author__ = 'Nikolay Dolganov'

app_name = 'codex_users'

urlpatterns = [
    url(r'sign-in/?$', SignInAPIView.as_view(), name='sign_in'),
    url(r'logout/?$', LogOutAPIView.as_view(), name='logout'),
]
