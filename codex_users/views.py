from codex_users.serializers import SignInSerializer, UserAuthSerializer
from rest_framework import views
from django.contrib.auth import login, logout
from rest_framework.permissions import AllowAny
from rest_framework import permissions
from rest_framework.response import Response
from django.apps import apps

__author__ = 'Nikolay Dolganov'
Token = apps.get_model('authtoken', 'Token')


class UnauthorizedOnly(permissions.BasePermission):
    """
    Allow access for unauthenticated users only
    """

    def has_permission(self, request, view):
        return not request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        return not request.user.is_authenticated


class SignInAPIView(views.APIView):
    """
    Sign-In API View
    """

    serializer_class = SignInSerializer
    permission_classes = [UnauthorizedOnly]

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.user
            login(request, user)
            if not hasattr(user, 'auth_token'):
                Token.objects.create(user=user)
            resp_serializer = UserAuthSerializer(user)
            return Response(resp_serializer.data)


class LogOutAPIView(views.APIView):
    """
    Logout API View
    """
    permission_classes = [AllowAny]

    def get(self, request):
        logout(request)
        return Response({'logout': True})
