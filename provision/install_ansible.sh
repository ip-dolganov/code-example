#!/bin/bash


sudo apt-get update
sudo apt-get install -y python3.5 build-essential libssl-dev libffi-dev python3.5-dev python3-pip python3-ndg-httpsclient
sudo pip3 install -U pip setuptools
sudo pip3 install ansible==2.3.0
sudo cp /usr/local/bin/ansible /usr/bin/ansible
sudo ln -s /usr/bin/python3 /usr/bin/python

mkdir -p ~/ansible/
cat > ~/ansible/.ansible.cfg <<EOF
[defaults]
remote_tmp = /vagrant/ansible/tmp
log_path = /vagrant/ansible/ansible.log
EOF