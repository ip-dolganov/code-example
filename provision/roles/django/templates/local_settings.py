import os


__author__ = 'Nikolay Dolganov'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


DEBUG = {% if global_debug |default('true')|bool %}True{% else %}False{% endif %}

VAGRANT_MODE = {% if vagrant_mode |default('true')|bool %}True{% else %}False{% endif %}

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [{{ domains_django }}]

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ pg_db_name }}',
        'USER': '{{ pg_db_user }}',
        'PASSWORD': '{{ pg_db_password }}'
    }
}

# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = '{{ static_path }}'

STATICFILES_DIRS = [{{ static_dirs if static_dirs is defined else '' }}]

STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

MEDIA_ROOT = '{{ media_path }}'
MEDIA_URL = '/media/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'frontend/templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

PROJECT_DOMAIN = '{{ site_domain }}'

# Local Email Backend
EMAIL_BACKEND = '{% if vagrant_mode | default("false") | bool %}django.core.mail.backends.filebased.EmailBackend{% else %}django.core.mail.backends.smtp.EmailBackend{% endif %}'
EMAIL_FILE_PATH = '{{ mail_dir }}'

# Server Email Backend
EMAIL_USE_TLS = True
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = 587
SERVER_EMAIL = ''
DEFAULT_FROM_MAIL = ''


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        },
    },
    'formatters': {
        'standard': {
            'format': "django [%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'syslog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.SysLogHandler',
            'facility': 'local7',
            'formatter': 'standard',
            'address': '/dev/log',
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join('{{ log_dir }}', 'main.log'),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'cron_logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join('{{ log_dir }}', 'cron.log'),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        {% if vagrant_mode |default('true')|bool  %}'sql_queries': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join('{{ log_dir }}', 'sql_queries.log'),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },{% endif %}
    },
    'loggers': {
        'django': {
            'handlers': ['syslog', 'console'],
            'propagate': True,
        },
        'django.db.backends': {
            'handlers': ['sql_queries'],
            'level': 'DEBUG',
        }
    }
}

__all__ = ['DATABASES', 'DEBUG', 'STATIC_URL', 'STATIC_ROOT', 'STATICFILES_DIRS', 'STATICFILES_FINDERS', 'MEDIA_ROOT',
           'MEDIA_URL', 'TEMPLATES', 'ALLOWED_HOSTS', 'PROJECT_DOMAIN', 'EMAIL_BACKEND',
           'EMAIL_FILE_PATH', 'EMAIL_USE_TLS', 'EMAIL_HOST', 'EMAIL_HOST_USER', 'EMAIL_HOST_PASSWORD', 'EMAIL_PORT',
           'SERVER_EMAIL', 'DEFAULT_FROM_MAIL', 'VAGRANT_MODE', 'LOGGING']
